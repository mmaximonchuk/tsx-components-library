import React from 'react';
import AuthRouter from '../AuthRouter';
import '../../assets/styles/auth.scss';

function Auth() {
  return <AuthRouter />
}

export default Auth;
