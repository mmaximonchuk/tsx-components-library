import React from "react";

import "../../assets/styles/components/buttons.scss";

function Button({ btnType, children, className = "", ...restProps }) {
  switch (btnType) {
    case "bordered":
      return (
        <button {...restProps} className={`btn btn--bordered ${className}`}>
          {children}
        </button>
      );
    case "solid":
    return (
      <button {...restProps} className={`btn btn--solid ${className}`}>
          {children}
        </button>
    );
    default:
      return (
        <button {...restProps} className={`btn btn--solid ${className}`}>
          {children}
        </button>
      );
  }
}

export default Button;
