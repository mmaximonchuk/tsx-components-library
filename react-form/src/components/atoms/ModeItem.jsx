import React from 'react';


function ModeItem({img, text}) {
  return (
    <button className="mode-item">
      <img className="icon" src={img} alt="" />
      <span>{text}</span>
    </button>
  );
}

export default ModeItem;
