import React from "react";
import { Route, Redirect } from "react-router-dom";
import { ROUTES } from "../routes";
import { MAN_PROFILE_ROUTE, MAN_PROFILE_EDIT_ROUTE } from "../utils/constants";

function AuthRouter() {
  return (
    <>
      {ROUTES.map((route) => {
        return (
          <Route key={route.id} path={route.path} component={route.component}/>
        );
      })}
     <Redirect to={MAN_PROFILE_ROUTE}/>
    </>
  );
}

export default AuthRouter;
