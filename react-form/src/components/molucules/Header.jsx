import React, { useState } from "react";
import { NavLink } from "react-router-dom";
import Button from "../atoms/Button";
import useResizeScreen from "../../hooks/useResizeScreen";
import imgLogo from "../../assets/images/layout/Лого.png";
import imgUser from "../../assets/images/layout/Avatar.png";
import imgSettings from "../../assets/images/layout/Settings.png";
import imgLogOut from "../../assets/images/layout/LoginOut.png";

import "../../assets/styles/components/header.scss";

function Header() {
  const [isMenuOpen, setMenuOpen] = useState(false);
  const { isMobile } = useResizeScreen();

  return (
    <header className={`header ${isMobile ? "header--mobile" : ""}`}>
      <div className="container inner">
        <img className="logo" src={imgLogo} alt="Logo" />
        <HeaderMenu />
        {isMenuOpen && isMobile && <HeaderMobileMenu />}
        {isMobile && <div className="toggle">
          <button onClick={() => setMenuOpen(!isMenuOpen)}>&times;</button>
        </div>}
      </div>
    </header>
  );
}

export default Header;

const HeaderMenu = () => {
  return (
    <div className="profile">
      <NavLink to="/man-profile/" className="user">
        <img src={imgUser} alt="Avatar" />
        <span>Bradley Pitt</span>
      </NavLink>
      <Button btnType="bordered">
        <img src={imgSettings} alt="imgSettings" />
        <span>Settings</span>
      </Button>
      <Button>
        <span>Login Out</span>
        <img src={imgLogOut} alt="imgLogOut" />
      </Button>
    </div>
  );
};

const HeaderMobileMenu = () => {
  return (
    <div className="profile-mobile">
      <NavLink to="/man-profile/" className="user">
        <img src={imgUser} alt="Avatar" />
        <span>Bradley Pitt</span>
      </NavLink>
      <Button btnType="bordered">
        <img src={imgSettings} alt="imgSettings" />
        <span>Settings</span>
      </Button>
      <Button>
        <span>Login Out</span>
        <img src={imgLogOut} alt="imgLogOut" />
      </Button>
    </div>
  );
};
