import {BrowserRouter as Router, Switch} from 'react-router-dom'
import Layout from './components/organism/Layout';
import Auth from './components/pages/Auth';

function App() {
  return (
    <Router className="App">
      <Switch>
        <Layout>
          <Auth />
        </Layout>
      </Switch>
    </Router>
  );
}

export default App;
