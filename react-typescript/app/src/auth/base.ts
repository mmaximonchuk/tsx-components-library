import { initializeApp } from "firebase/app";
import { getFirestore } from "firebase/firestore";
import { getStorage } from "firebase/storage";
import { getAuth } from "firebase/auth";
import "firebase/database";

const firebaseApp = initializeApp({
  apiKey: "AIzaSyBB4zPhpCSsp4dPblr9YbDK5wLJxBoKYd8",
  authDomain: "react-test-158ab.firebaseapp.com",
  databaseURL: "https://react-test-158ab-default-rtdb.firebaseio.com",
  projectId: "react-test-158ab",
  storageBucket: "react-test-158ab.appspot.com",
  messagingSenderId: "131642063636",
  appId: "1:131642063636:web:d52d70629185e37f086001",
  measurementId: "G-439SP0H2GH"
});



export const auth = getAuth(firebaseApp); 
export const db = getFirestore(firebaseApp); 
export const storage = getStorage(firebaseApp); 



export { firebaseApp };

// export default base;
