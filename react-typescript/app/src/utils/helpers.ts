// enum LocalStorageRecords {
//   LastVisitedStep = "lastVisitedStep",
//   LastVisitedQuiz = "lastVisitedQuiz",
// }

import { FirebaseError } from "firebase/app";

// export const handleRecordLastStage = (step: number | null, quizId?: number): void => {
//   if (step === null || !Boolean(quizId)) {
//     localStorage.removeItem(LocalStorageRecords.LastVisitedStep);
//     localStorage.removeItem(LocalStorageRecords.LastVisitedQuiz);
//   } else {
//     localStorage.setItem(LocalStorageRecords.LastVisitedStep, JSON.stringify(step));
//     localStorage.setItem(LocalStorageRecords.LastVisitedQuiz, JSON.stringify(quizId));
//   }
// };

// export const handleClearLastStage = (): void => {
//   handleRecordLastStage(null);
// };

export const getErrorMessage = (e: unknown) => {
  let message = "Unknown Error";
  if (e instanceof FirebaseError || e instanceof Error) message = e.message;
  return message;
};
