export interface iQuizData extends Record<string, unknown>{
  _id: number;
  // currentStep: number | null;
  quizTitle: string;
  quizDescription: string;
  questions: iQuizQuestion[];
}

export interface iQuizQuestion {
  _id: number;
  isMulti: boolean;
  question: string;
  isSkipable?: boolean;
  answers: iQuizOfferedAnswer[];
}


export interface iQuizOfferedAnswer {
  _id: number;
  text: string;
  isValid: boolean;
}

export const quizData: iQuizData[] = [{
  _id: 1,
  quizTitle: 'First quiz 1',
  quizDescription: 'first quiz description ...',
  currentStep: null,
  questions: [
    {
      _id: 1,
      isMulti: false,
      question: "What do you want to?",
      answers: [
        {
          _id: 1,
          text: "a1",
          isValid: false,
        },
        {
           _id: 2,
          text: "a2",
          isValid: false,
        },
        {
           _id: 3,
          text: "a3",
          isValid: false,
        },
        {
           _id: 4,
          text: "a4",
          isValid: false,
        },
        {
           _id: 5,
          text: "a5",
          isValid: true,
        },
      ],
    },
    {
      _id: 2,
      isMulti: false,
      question: "What do you want to?2",
      answers: [
        {
          _id: 1,
          text: "a21",
          isValid: false,
        },
        {
          _id: 2,
          text: "a22",
          isValid: true,
        },
        {
          _id: 3,
          text: "a23",
          isValid: false,
        },
        {
          _id: 4,
          text: "a24",
          isValid: false,
        },
        {
          _id: 5,
          text: "a25",
          isValid: false,
        },
      ],
    },
    {
      _id: 3,
      isMulti: true,
      question: "What do you want to?3",
      isSkipable: true,
      answers: [
        {
          _id: 1,
          text: "a31",
          isValid: false,
        },
        {
          _id: 2,
          text: "a32",
          isValid: true,
        },
        {
          _id: 3,
          text: "a33",
          isValid: false,
        },
        {
          _id: 4,
          text: "a34",
          isValid: true,
        },
        {
          _id: 5,
          text: "a35",
          isValid: false,
        },
      ],
    },
  ],
},
{
  _id: 2,
  quizTitle: 'Second quiz 2',
  quizDescription: 'second quiz description ...',
  currentStep: null,
  questions: [
    {
      _id: 1,
      isMulti: false,
      question: "What do you want to 2?",
      answers: [
        {
          _id: 1,
          text: "s1",
          isValid: false,
        },
        {
           _id: 2,
          text: "s2",
          isValid: false,
        },
        {
           _id: 3,
          text: "s3",
          isValid: false,
        },
        {
           _id: 4,
          text: "s4",
          isValid: false,
        },
        {
           _id: 5,
          text: "s5",
          isValid: true,
        },
      ],
    },
    {
      _id: 2,
      isMulti: false,
      question: "What do you want to?2",
      answers: [
        {
          _id: 1,
          text: "s21",
          isValid: false,
        },
        {
          _id: 2,
          text: "s22",
          isValid: true,
        },
        {
          _id: 3,
          text: "a23",
          isValid: false,
        },
        {
          _id: 4,
          text: "s24",
          isValid: false,
        },
        {
          _id: 5,
          text: "s25",
          isValid: false,
        },
      ],
    },
    {
      _id: 3,
      isMulti: true,
      question: "What do you want to?3",
      isSkipable: true,
      answers: [
        {
          _id: 1,
          text: "s31",
          isValid: false,
        },
        {
          _id: 2,
          text: "s32",
          isValid: true,
        },
        {
          _id: 3,
          text: "s33",
          isValid: false,
        },
        {
          _id: 4,
          text: "s34",
          isValid: true,
        },
        {
          _id: 5,
          text: "s35",
          isValid: false,
        },
      ],
    },
  ],
}];