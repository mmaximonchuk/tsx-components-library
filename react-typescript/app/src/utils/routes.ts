export enum Routes {
  HomePage = "/",
  Login = "/login",
  Register = "/register",
  QuizStep = "/:quizId/step/:stepId",
  ResultsPage = "/results",
  SettingsPage = "/settings",
  BlogPage = "/blog",
  BlogPostPage = "/blog/:postId",
}
