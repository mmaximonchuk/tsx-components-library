export const DEFAULT_FONT_SIZE = 14;
export const DEFAULT_BG_COLOR = "#e5e5e5";
export const DEFAULT_FONT_COLOR = "rgba(0,0,0,1)";
export const SUCCESSFULLY_LOGGED_OUT_MESSAGE = "You was successfully logged out!";
export const SUCCESSFULLY_LOGGED_IN_MESSAGE = "You was successfully logged in!";
