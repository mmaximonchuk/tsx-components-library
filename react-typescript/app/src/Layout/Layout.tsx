import { Header, ProgressBar } from "./";

import { LayoutProps } from "./Layout.props";

import { useAppSelector } from "../hooks/useStore";

import s from "./Layout.module.scss";

function Layout({ children, bgColor = "#fff" }: LayoutProps) {
  const userProgress = useAppSelector((state) => state.quiz.userProgress);

  // const calcUserProgress = useCallback((): number | null => {
  //     if (!(currentStep && currentQuizData)) return null;

  //     const calculatedProgress = (currentStep / (currentQuizData.find(q => q._id === currentQuizId)?.questions?.length ?? 1)) * 100;
  //     return calculatedProgress;
  // }, [currentStep, currentQuizData])

  const showProgressBar = userProgress !== null;
  return (
    <div className={s.appLayout} style={{ background: bgColor }}>
      <div className={s.layoutContainer}>
        <ProgressBar showProgressBar={showProgressBar} userProgress={userProgress} />
        <Header />
        <main className={s.main}>{children}</main>
      </div>
    </div>
  );
}

export default Layout;
