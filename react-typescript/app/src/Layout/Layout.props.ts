import { DetailedHTMLProps, HtmlHTMLAttributes, PropsWithChildren } from "react";

export interface LayoutProps
  extends PropsWithChildren<DetailedHTMLProps<HtmlHTMLAttributes<HTMLDivElement>, HTMLDivElement>> {
  bgColor?: string;
}
