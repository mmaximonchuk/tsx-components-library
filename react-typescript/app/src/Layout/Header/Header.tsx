import { useState, useEffect, KeyboardEvent } from "react";
import { Link, useLocation } from "react-router-dom";
import { message } from "antd";
import { getAuth } from "firebase/auth";

import { firebaseApp } from "../../auth/base";

import { DropdownMenu } from "../../components";
import { Loader } from "../../components/loaders/Loader";

import { useAppDispatch, useAppSelector } from "../../hooks/useStore";

import { setAuthStatus, setLogOut } from "../../store/userSlice/userReducer";
import { Routes } from "../../utils/routes";
import { getErrorMessage } from "../../utils/helpers";

import imgAvatar from "../../assets/images/avatar.png";
import imgLogo from "../../assets/images/puzzle-piece.png";

import s from "./Header.module.scss";

function Header() {
  const [messageApi, contextHolder] = message.useMessage();
  const [isVisibleMenu, setIsVisibleMenu] = useState<boolean>(false);
  const dispatch = useAppDispatch();
  const user = useAppSelector((state) => state.user.user);
  const status = useAppSelector((state) => state.user.status);
  const error = useAppSelector((state) => state.user.error);
  const location = useLocation();

  const handleLogOut = async () => {
    try {
      await new Promise<void>((res, rej) => {
        try {
          getAuth(firebaseApp).signOut();
          res();
        } catch (e) {
          rej(e);
        }
      });
      dispatch(setLogOut());
    } catch (e) {
      const message = getErrorMessage(e);
      dispatch(setAuthStatus({ status: "error", error: message }));
    }
  };

  useEffect(() => {
    if (!error) return;

    messageApi.open({ type: "error", content: error });
  }, [error, messageApi]);

  useEffect(() => {
    closeMenu();
  }, [location.pathname]);

  const toggleMenu = () => setIsVisibleMenu((prev) => !prev);
  const handleKeyDown = (event: KeyboardEvent) => {
    // console.log('event: ', event.key); ?? КАКОГО ХРЕНА ЕНТЕР НЕ РАБОТАЕТ?????
    if (event.key === "Escape" || event.key === "Space") {
      toggleMenu();
    }
  };
  const closeMenu = () => setIsVisibleMenu(false);

  const showLoader = status === "loading";
  const showUserThumb = user !== null;
  const userAvatar = user?.photoURL;
  const userName = user?.displayName?.split(" ")[0];
  const userSurname = user?.displayName?.split(" ")[1];
  return (
    <header className={s.header}>
      <div className={s.headerInner}>
        <div className={s.logo}>
          <Link to={Routes.HomePage}>
            <img src={imgLogo} alt="QuizLetty logo" />
          </Link>
        </div>
        {showLoader && <Loader type="fast-spinner" />}
        {showUserThumb ? (
          <button onClick={toggleMenu} onKeyDown={handleKeyDown} className={s.userProfile}>
            <img src={userAvatar ?? undefined} alt="user avatar" />
            <span>
              {userName} {userSurname}
            </span>
          </button>
        ) : (
          <button className={s.signInBtn}>Sign In</button>
        )}
        <DropdownMenu isVisibleMenu={isVisibleMenu} onLogout={handleLogOut} />
      </div>
      {contextHolder}
    </header>
  );
}

export default Header;
