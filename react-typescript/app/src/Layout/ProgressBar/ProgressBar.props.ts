import { DetailedHTMLProps, HTMLAttributes } from "react";

export interface ProgressBarProps extends DetailedHTMLProps<HTMLAttributes<HTMLDivElement>,HTMLDivElement> {
    userProgress: number | null;
    showProgressBar: boolean;
}