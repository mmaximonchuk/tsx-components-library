import cn from "classnames";

import { ProgressBarProps } from "./ProgressBar.props";

import s from "./ProgressBar.module.scss";

function ProgressBar({ userProgress, showProgressBar }: ProgressBarProps) {
  return (
    <div
      className={cn(s.progressBar, {
        [s.visible]: showProgressBar,
      })}
      style={{ width: `${userProgress}%` }}
    />
  );
}

export default ProgressBar;
