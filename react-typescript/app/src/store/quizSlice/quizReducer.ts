import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { QuizReducer } from "../../domain/QuizReducer.interface";
import { iQuizData } from "../../utils/mockQuizData";


export type setStepAction = { stepId: number | null };
export type setUserAnswerAction = { currentStep: number | null; answerId: number };
export type setCurrentQuizIdAction = { quizId: number | null };
export type setIsFetchingAction = { isFetching: boolean };
export type setCurrentQuizDataAction = { quizData: iQuizData[] };

const initialState = {
  currentStep: null,
  currentQuizId: null,
  isFetching: false,
  userProgress: null,
  currentQuizData: null,
  currentUserAnswers: [],
} as QuizReducer;


const quizSlice = createSlice({
  name: "quiz",
  initialState,
  reducers: {
    setStep: (state, { payload: { stepId } }: PayloadAction<setStepAction>) => {
      state.currentStep = stepId;
    },
    setUserAnswer: (state, { payload: { currentStep, answerId } }: PayloadAction<setUserAnswerAction>) => {
      state.currentUserAnswers = [
        ...state.currentUserAnswers,
        {
          _id: new Date().getMinutes(),
          quizId: state.currentQuizId,
          answers: [],
        },
      ];
      const updatedAnswer = state.currentUserAnswers.find(
        (answerEntity) => answerEntity.quizId === state.currentQuizId
      );
      const isMultiQuiz =
        currentStep !== null &&
        state.currentQuizId !== null &&
        state.currentQuizData?.[state.currentQuizId]?.questions?.[currentStep - 1].isMulti;
      const currentSelectedAnswersLength =
        updatedAnswer && currentStep && updatedAnswer.answers[currentStep - 1]?.answersId?.length;
      const currentSelectedAnswers = updatedAnswer && currentStep && updatedAnswer.answers[currentStep - 1];

      const canSetSomeAnswers = currentSelectedAnswers && currentSelectedAnswersLength && isMultiQuiz;

      if (!updatedAnswer) return;

      const isTouchedStep = updatedAnswer.answers.find((answer) => answer.stepId === currentStep);

      if (isTouchedStep) {
        updatedAnswer.answers = updatedAnswer.answers.map((answer) => {
          if (answer.stepId === isTouchedStep.stepId) {
            return {
              _id: new Date().getMilliseconds(),
              stepId: currentStep,
              answersId: canSetSomeAnswers
                ? currentSelectedAnswers.answersId.includes(answerId)
                  ? currentSelectedAnswers.answersId.filter((answer) => answer !== answerId)
                  : [...currentSelectedAnswers.answersId, answerId]
                : [answerId],
            };
          }
          return answer;
        });
      } else {
        updatedAnswer.answers = [
          ...updatedAnswer.answers,
          {
            _id: new Date().getMilliseconds(),
            stepId: currentStep,
            answersId: canSetSomeAnswers ? [...currentSelectedAnswers.answersId, answerId] : [answerId],
          },
        ];
      }

      const newUserAnswers = state.currentUserAnswers.some((answer) => answer.quizId === state.currentQuizId)
        ? [updatedAnswer]
        : [...state.currentUserAnswers, updatedAnswer];

      state.currentUserAnswers = newUserAnswers;
    },
    setCurrentQuizData: (state, { payload: { quizData } }: PayloadAction<setCurrentQuizDataAction>) => {
      state.currentQuizData = quizData;
    },
    setCurrentQuizId: (state, { payload: { quizId } }: PayloadAction<setCurrentQuizIdAction>) => {
      state.currentQuizId = quizId;
    },
    setIsFetching: (state, { payload: { isFetching } }: PayloadAction<setIsFetchingAction>) => {
      state.isFetching = isFetching;
    },
    setUserProgress: (state) => {
      if (!(state.currentStep && state.currentQuizData)) {
        state.userProgress = null;
        return;
      }

      const calculatedProgress =
        (state.currentStep /
          (state.currentQuizData.find((q) => q._id === state.currentQuizId)?.questions?.length ?? 1)) *
        100;

      state.userProgress = calculatedProgress;
    },
  },
  extraReducers: (builder) => {},
});

export const { setUserProgress, setCurrentQuizData, setCurrentQuizId, setIsFetching, setStep, setUserAnswer } =
  quizSlice.actions;

export const quizReducer = quizSlice.reducer;
