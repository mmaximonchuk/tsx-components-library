import { createAsyncThunk, createSlice, isAnyOf, PayloadAction } from "@reduxjs/toolkit";
import { nanoid } from "nanoid";
import {
  createUserWithEmailAndPassword,
  GithubAuthProvider,
  signInWithEmailAndPassword,
  signInWithPopup,
  updateProfile,
} from "firebase/auth";
import { doc, setDoc } from "firebase/firestore";
import { getDownloadURL, ref, uploadBytesResumable } from "firebase/storage";

import { auth, db, storage } from "../../auth/base";

import { Nullable } from "../../domain";
import { AuthStatuses } from "../../domain";
import { iUser } from "../../domain/iUser.interface";

// import { DEFAULT_BG_COLOR, DEFAULT_FONT_COLOR, DEFAULT_FONT_SIZE } from "../../utils/constants";

import { getErrorMessage } from "../../utils/helpers";

export type setBgColorAction = { bgColor: string };
export type setFontColorAction = { fontColor: string };
export type setFontSizeAction = { fontSize: number };
export type setAuthStatusAction = { status: AuthStatuses; error?: string };
export type setUserAction = { user: iUser };

export type SignUpRequest = {
  email: string;
  password: string;
  name: string;
  file: File;
};

export type SignInRequest = {
  email: string;
  password: string;
};

export const signUpUserRequest = createAsyncThunk(
  "user/signUp",
  async ({ email, password, name, file }: SignUpRequest, { rejectWithValue }) => {
    try {
      const response = await createUserWithEmailAndPassword(auth, email, password);
      const user = response.user;

      const storageRef = ref(storage, `images/${nanoid(10) + name}`);
      const uploadTask = uploadBytesResumable(storageRef, file);

      const downloadURL = await new Promise<string>((resolve, reject) => {
        uploadTask.on("state_changed", null, null, async () => {
          try {
            const downloadURL = await getDownloadURL(uploadTask.snapshot.ref);
            await setDoc(doc(db, "users", user.uid), {
              uid: user.uid,
              displayName: name,
              email,
              photoURL: downloadURL,
            });
            await updateProfile(user, {
              photoURL: downloadURL,
              displayName: name,
            });

            resolve(downloadURL);
          } catch (error) {
            reject(error);
          }
        });
      });
      const serializableUserData: iUser = {
        uid: user.uid,
        email: user.email,
        displayName: user.displayName,
        photoURL: downloadURL,
      };
      return { user: serializableUserData };
    } catch (err) {
      return rejectWithValue(err);
    }
  }
);

export const signInUserRequest = createAsyncThunk(
  "user/signIn",
  async ({ email, password }: SignInRequest, { rejectWithValue }) => {
    try {
      const { user } = await signInWithEmailAndPassword(auth, email, password);
      const serializableUserData: iUser = {
        uid: user.uid,
        email: user.email,
        displayName: user.displayName,
        photoURL: user.photoURL,
      };
      return { user: serializableUserData };
    } catch (err) {
      return rejectWithValue(err);
    }
  }
);

export const signInWithGithubRequest = createAsyncThunk("user/signInWithGithub", async (_, { rejectWithValue }) => {
  // try {
  //   const { user } = await signInWithEmailAndPassword(auth, email, password);
  //   const serializableUserData: iUser = {
  //     uid: user.uid,
  //     email: user.email,
  //     displayName: user.displayName,
  //     photoURL: user.photoURL,
  //   };
  //   return { status: "resolved" as AuthStatuses, user: serializableUserData };
  // } catch (err) {
  //   return rejectWithValue(err);
  // }

  const provider = new GithubAuthProvider();

  try {
    const result = await signInWithPopup(auth, provider);
    const user = result.user;
    // const credential = GithubAuthProvider.credentialFromResult(result);
    // const token = credential?.accessToken;

    // The signed-in user info.

    await setDoc(doc(db, "users", user.uid), {
      uid: user.uid,
      displayName: user.displayName,
      email: user.email,
      photoURL: user.photoURL,
    });
    await updateProfile(user, {
      photoURL: user.photoURL,
      displayName: user.displayName,
    });
    const serializableUserData: iUser = {
      uid: user.uid,
      email: user.email,
      displayName: user.displayName,
      photoURL: user.photoURL,
    };
    return { user: serializableUserData };
  } catch (error) {
    return rejectWithValue(error);
  }
});

const initialState = {
  // fontSize: Number.parseInt(JSON.parse(localStorage.getItem("font-size") ?? DEFAULT_FONT_SIZE.toString())),
  // fontColor: JSON.parse(localStorage.getItem("font-color") ?? JSON.stringify(DEFAULT_FONT_COLOR)),
  // bgColor: JSON.parse(localStorage.getItem("bg-color") ?? JSON.stringify(DEFAULT_BG_COLOR)),
  fontSize: 16,
  fontColor: "",
  bgColor: "",
  error: null as Nullable<string>,
  status: "idle" as AuthStatuses,
  user: null as Nullable<iUser>,
  authenticated: false,
};

type UserSettingsStateType = typeof initialState;

const userSlice = createSlice({
  name: "user",
  initialState,
  reducers: {
    setBgColor: (state, { payload: { bgColor } }: PayloadAction<setBgColorAction>) => {
      localStorage.setItem("bg-color", JSON.stringify(bgColor));
      state.bgColor = bgColor;
    },
    setFontColor: (state, { payload: { fontColor } }: PayloadAction<setFontColorAction>) => {
      localStorage.setItem("font-color", JSON.stringify(fontColor));
      state.fontColor = fontColor;
    },
    setFontSize: (state, { payload: { fontSize } }: PayloadAction<setFontSizeAction>) => {
      localStorage.setItem("font-size", JSON.stringify(fontSize.toString()));
      state.fontSize = fontSize;
    },
    setUser: (state, { payload: { user } }: PayloadAction<setUserAction>) => {
      state.user = user;
      state.authenticated = true;
    },
    setLogOut: (state) => {
      state.user = null;
      state.authenticated = false;
    },
    setAuthStatus: (state, { payload: { status, error } }: PayloadAction<setAuthStatusAction>) => {
      state.status = status;
      if (error) {
        state.error = error;
      }
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(signUpUserRequest.fulfilled, (state, { payload: { user } }) => {
        state.error = null;
        state.user = user;
        state.status = "resolved";
        state.authenticated = true;
      })
      .addCase(signInUserRequest.fulfilled, (state, { payload: { user } }) => {
        state.error = null;
        state.user = user;
        state.status = "resolved";
        state.authenticated = true;
      })
      .addCase(signInWithGithubRequest.fulfilled, (state, { payload: { user } }) => {
        state.error = null;
        state.user = user;
        state.status = "resolved";
        state.authenticated = true;
      })

      .addMatcher(
        isAnyOf(signInUserRequest.pending, signUpUserRequest.pending, signInWithGithubRequest.pending),
        (state) => {
          state.error = null;
          state.status = "loading";
        }
      )
      .addMatcher(
        isAnyOf(signInUserRequest.rejected, signUpUserRequest.rejected, signInWithGithubRequest.rejected),
        (state, { payload }) => {
          const message = getErrorMessage(payload);

          state.error = message;
          state.status = "error";
        }
      );
  },
});

export const { setBgColor, setFontColor, setFontSize, setAuthStatus, setLogOut, setUser } = userSlice.actions;

export const userReducer = userSlice.reducer;
