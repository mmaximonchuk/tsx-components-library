import { Nullable } from ".";
import { iQuizData } from "../utils/mockQuizData";
import { iAnswers } from "./iUser.interface";

export interface QuizReducer {
  currentQuizData: Nullable<iQuizData[]>;
  currentStep: Nullable<number>;
  currentQuizId: Nullable<number>;
  userProgress: Nullable<number>;
  isFetching: boolean;
  currentUserAnswers: iAnswers[];
}
