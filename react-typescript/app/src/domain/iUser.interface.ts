import { User } from "firebase/auth";

export interface iUser extends Pick<User, "email" | "displayName" | "photoURL" | "uid"> {}

export interface iAnswers {
  _id: number;
  quizId: number | null;
  answers: iAnswer[];
}

export interface iAnswer {
  _id: number;
  stepId: number | null;
  answersId: number[];
}
