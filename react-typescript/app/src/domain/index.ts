/* eslint-disable */

export type Nullable<T> = T | null;

export type AuthStatuses = "idle" | "loading" | "resolved" | "error";

export enum AuthAction {
  Login = "login",
  Register = "register",
}
