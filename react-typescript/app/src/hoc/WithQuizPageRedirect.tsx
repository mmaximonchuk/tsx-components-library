import React from "react";
import { useHistory } from "react-router-dom";
import { IQuizStep } from "../pages/Step/QuizStep.props";

function withQuizPageRedirect<T extends Record<string, unknown> & IQuizStep>(Component: React.FC<T>) {
  const ComponentWithRedirect = (props: T): JSX.Element => {
    const history = useHistory();

    // useEffect(() => {
    //   if (!currentQuizData) {
    //     history.push(Routes.HomePage);
    //   }
    // }, [currentQuizData, history]);

    return <Component {...props} />;
  };

  return ComponentWithRedirect;
}

export default withQuizPageRedirect;
