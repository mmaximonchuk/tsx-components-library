import React from "react";
import { IQuizStep } from "../pages/Step/QuizStep.props";
import { useAppSelector } from "../hooks/useStore";
import { Redirect } from "react-router-dom";
import { Routes } from "../utils/routes";

function withAuthRedirect<T extends Record<string, unknown> & IQuizStep>(Component: React.FC<T>) {
  const ComponentWithRedirect = (props: T): JSX.Element => {
    const authenticated = useAppSelector((state) => state.user.authenticated);

    return authenticated ? <Component {...props} /> : <Redirect to={Routes.Login} />;
  };

  return ComponentWithRedirect;
}

export default withAuthRedirect;
