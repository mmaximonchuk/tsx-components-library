import { FormEvent, useRef, useState, useEffect, Suspense } from "react";
import { useHistory } from "react-router-dom";

import { Input, InputRef, Upload, message } from "antd";
import { PlusOutlined, MailOutlined, KeyOutlined } from "@ant-design/icons";
import { UserOutlined } from "@ant-design/icons";
import { nanoid } from "nanoid";
// import { AvatarGenerator } from "random-avatar-generator";

import { signInUserRequest, signInWithGithubRequest } from "../../store/userSlice/userReducer";

import { RcFile, UploadChangeParam, UploadFile, UploadProps } from "antd/es/upload";
import { AuthAction } from "../../domain";

import { useAppDispatch, useAppSelector } from "../../hooks/useStore";
// import { SUCCESSFULLY_LOGGED_IN_MESSAGE, SUCCESSFULLY_LOGGED_OUT_MESSAGE } from "../../utils/constants";

import { signUpUserRequest } from "../../store/userSlice/userReducer";
import { Routes } from "../../utils/routes";
import { SUCCESSFULLY_LOGGED_OUT_MESSAGE } from "../../utils/constants";

import mockUserAvatar from "../../assets/images/avatar.png";

import css from "./Login.module.scss";
import { Loader, PanoramicBG } from "../../components";

const getBase64 = (img: RcFile, callback: (url: string) => void) => {
  const reader = new FileReader();
  reader.addEventListener("load", () => callback(reader.result as string));
  reader.readAsDataURL(img);
};

const beforeUpload = (file: RcFile) => {
  const isJpgOrPng = file.type === "image/jpeg" || file.type === "image/png";
  if (!isJpgOrPng) {
    message.error("You can only upload JPG/PNG file!");
  }
  const isLt2M = file.size / 1024 / 1024 < 2;
  if (!isLt2M) {
    message.error("Image must smaller than 2MB!");
  }
  return isJpgOrPng && isLt2M;
};

export default function Login(): JSX.Element {
  const dispatch = useAppDispatch();
  const history = useHistory();
  const currentUser = useAppSelector((state) => state.user.user);
  const authenticated = useAppSelector((state) => state.user.authenticated);
  const status = useAppSelector((state) => state.user.status);
  const error = useAppSelector((state) => state.user.error);
  const [file, setFile] = useState<File | null>(null);
  const [imageUrl, setImageUrl] = useState<string | null>(null);
  const [authAction, setAuthAction] = useState<AuthAction>(AuthAction.Login);

  const nameInputRef = useRef<InputRef>(null);
  const emailInputRef = useRef<InputRef>(null);
  const passwordInputRef = useRef<InputRef>(null);

  const handleChange: UploadProps["onChange"] = (info: UploadChangeParam<UploadFile>) => {
    setFile(info.file.originFileObj as RcFile);
  };

  useEffect(() => {
    if (!file) return;

    getBase64(file as RcFile, (url) => {
      setImageUrl(url);
    });
  }, [file]);

  useEffect(() => {
    if (currentUser === null && !authenticated && status === "resolved") {
      message.success(SUCCESSFULLY_LOGGED_OUT_MESSAGE);
    }
    if (authenticated) return history.push(Routes.HomePage);
  }, [authenticated, currentUser, status, error, history]);

  const signInWithGithub = () => {
    dispatch(signInWithGithubRequest());
  };

  const handleSubmit = (event: FormEvent, action: AuthAction) => {
    event.preventDefault();
    switch (action) {
      case AuthAction.Login: {
        const email = emailInputRef.current?.input?.value;
        const password = passwordInputRef.current?.input?.value;

        if (!email || !password) return;

        dispatch(signInUserRequest({ email, password }));

        return;
      }
      case AuthAction.Register: {
        const name = nameInputRef.current?.input?.value;
        const email = emailInputRef.current?.input?.value;
        const password = passwordInputRef.current?.input?.value;

        const mockFile = new File([mockUserAvatar], nanoid(10) + name);
        // const file = fileInputRef.current?.files?.[0] ?? mockFile;
        // const file = file ?? mockFile;
        const userAvatarFile = file ?? mockFile;

        if (!email || !password || !name) return;

        dispatch(
          signUpUserRequest({
            email,
            password,
            name,
            file: userAvatarFile,
          })
        );
        return;
      }
      default:
        return;
    }
  };

  const uploadButton = (
    <div>
      <PlusOutlined />
      <div style={{ marginTop: 8 }}>Upload</div>
    </div>
  );


  return (
    <div className={css.loginContainer}>
      <div className={css.loginContent}>
      <h2>Авторизация</h2>
      <button onClick={() => setAuthAction(AuthAction.Register)}>Зарегистрироватся</button>
      <button onClick={() => setAuthAction(AuthAction.Login)}>Войти</button>
      {authAction === AuthAction.Register && (
        <form className={css.form} onSubmit={(e) => handleSubmit(e, AuthAction.Register)}>
          <h3>Регистрация</h3>
          <label>
            <span>Name:</span>
            <Input
              size="large"
              prefix={<UserOutlined />}
              type="text"
              ref={nameInputRef}
              name="name"
              placeholder="Your name"
              required
            />
          </label>
          <label>
            <span>Email:</span>
            <Input
              size="large"
              prefix={<MailOutlined />}
              type="email"
              ref={emailInputRef}
              name="email"
              placeholder="your_email@gmail.com"
              required
            />
          </label>
          <label>
            <span>Password:</span>
            <Input
              size="large"
              prefix={<KeyOutlined />}
              type="password"
              ref={passwordInputRef}
              name="password"
              required
            />
          </label>
          <label>
            <span>Avatar:</span>

            <Upload
              name="avatar"
              listType="picture-circle"
              className="avatar-uploader"
              showUploadList={false}
              action="https://www.mocky.io/v2/5cc8019d300000980a055e76"
              beforeUpload={beforeUpload}
              onChange={handleChange}
            >
              {imageUrl !== null ? <img src={imageUrl} alt="avatar" style={{ width: "100%" }} /> : uploadButton}
            </Upload>
            {/* <input type="file" ref={fileInputRef} name="file" /> */}
          </label>
          <button type="submit">Register</button>
          <button type="button" className="github" onClick={signInWithGithub}>
            Sign In With GitHub
          </button>
        </form>
      )}
      {authAction === AuthAction.Login && (
        <form className={css.form} onSubmit={(e) => handleSubmit(e, AuthAction.Login)}>
          <h3>Вход</h3>
          <label>
            <span>Email:</span>
            <Input
              size="large"
              prefix={<MailOutlined />}
              type="email"
              ref={emailInputRef}
              name="email"
              placeholder="your_email@gmail.com"
              required
            />
          </label>
          <label>
            <span>Password:</span>
            <Input
              size="large"
              prefix={<KeyOutlined />}
              type="password"
              ref={passwordInputRef}
              name="password"
              required
            />
          </label>
          <button type="submit">Login</button>
          <button type="button" className="github" onClick={signInWithGithub}>
            Sign In With GitHub
          </button>
        </form>
      )}

      </div>
     
      <Suspense fallback={<Loader type="fast-spinner" />}>
        <PanoramicBG className={css.background} />
      </Suspense>
    </div>
  );
}
