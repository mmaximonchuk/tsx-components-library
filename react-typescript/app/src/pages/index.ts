export { default as HomePage } from "./HomePage/HomePage";
export { default as QuizStep } from "./Step/QuizStep";
export { default as Results } from "./Results/Results";
export { default as Login } from "./Auth/Login";
export { default as Settings } from "./Settings/Settings";
