import { Link } from "react-router-dom";
// import { Button } from "antd";
// import { Card } from "../../components";
import { useAppSelector } from "../../hooks/useStore";

import withAuthRedirect from "../../hoc/withAuthRedirect";

import { Loader } from "../../components";

import s from "./HomePage.module.scss";
import { Button } from "antd";
import { useEffect, useState } from "react";
import { db } from "../../auth/base";
import { Timestamp, collection, deleteDoc, doc, getDocs, onSnapshot, query, setDoc, where } from "firebase/firestore";

type Comment = {
  uid: string;
  userId: string;
  content: string;
  createdAt: Timestamp;
  title: string;
};

function HomePage(): JSX.Element {
  const currentQuizData = useAppSelector((state) => state.quiz.currentQuizData);
  const isFetching = useAppSelector((state) => state.quiz.isFetching);
  const user = useAppSelector((state) => state.user.user);

  const [comments, setComments] = useState<Comment[]>([]);

  useEffect(() => {
    const commentsRef = collection(db, "comments");
    const q = query(commentsRef, where("userId", "==", user?.uid));

    const listener = onSnapshot(q, (querySnapshot) => {
      const data = querySnapshot.docs.map((doc) => ({ ...(doc.data() as Comment), uid: doc.id }));
      console.log("data: ", data);
      setComments(data);
    });

    return () => listener();
  }, [user?.uid]);

  // const fetchComments = async () => {
  //   const commentsRef = collection(db, "comments");

  //   // Create a query against the collection.
  //   const q = query(commentsRef, where("userId", "==", user?.uid));
  //   const querySnapshot = await getDocs(q);

  //   const data = querySnapshot.docs.map((doc) => ({ ...(doc.data() as Comment), uid: doc.id }));
  //   console.log("data: ", data);
  //   setComments(data);
  // };

  const addComment = async () => {
    const comment = {
      userId: user?.uid,
      content: "some CONTENT",
      createdAt: Timestamp.fromDate(new Date()),
      title: `some comment of ${user?.displayName}`,
    };

    const commentRef = doc(db, "comments", Date.now().toString());
    await setDoc(commentRef, comment);
  };

  const deleteComment = async (uid: string) => {
    const commentRef = doc(db, "comments", uid);
    await deleteDoc(commentRef);
  };

  return (
    <div className={s.HomePage}>
      <h1 className={s.title}>QuizLetty</h1>
      {/* <Button onClick={fetchComments} type="primary">
        Get comments
      </Button> */}
      <Button onClick={addComment} type="default">
        Add Comment
      </Button>

      <h2>Comments</h2>
      <ul>
        {comments.map((comment) => (
          <li key={comment.uid}>
            <h3>{comment.title}</h3>
            <p>{comment.content}</p>
            <p>{comment.createdAt.toDate().toLocaleString()}</p>
            <hr />
            <Button onClick={() => deleteComment(comment.uid)} danger type="primary">
              Delete comment
            </Button>
          </li>
        ))}
      </ul>

      <p>Let's challange your knowelede in {"$topic"} topic</p>
      <h2>Available Quizes</h2>
      {isFetching && (
        <div>
          <Loader type="fast-spinner" />
        </div>
      )}
      <ul className={s.quizList}>
        {currentQuizData !== null &&
          currentQuizData.map((quiz) => {
            return (
              <li className={s.quizListItem} key={quiz._id}>
                <Link className={s.quizItemLink} to={`${quiz._id}/step/1`}>
                  <strong className={s.quizItemTitle}>{quiz.quizTitle}</strong>
                  <span className={s.quizItemDescription}>{quiz.quizDescription}</span>
                </Link>
              </li>
            );
          })}
        {!currentQuizData?.length && <li>There are no quizes yet!</li>}
      </ul>
      {/* <Link to={`/1/step/1`}>Go to quiz!</Link> */}
    </div>
  );
}

export default withAuthRedirect(HomePage);
