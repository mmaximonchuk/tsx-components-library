
import { Link } from "react-router-dom";

import withAuthRedirect from "../../hoc/withAuthRedirect";
import { iResultProps } from "./Results.props";

import s from "./Results.module.scss";

// import { useQuizContext } from '../../store/quizContext';

function Results({}: iResultProps): JSX.Element {
  // const { currentQuizId, isFetching, setStep, currentQuizData} = useQuizContext()


  return (
    <div className={s.wrapper}>
      Results
      <Link to={"/"}>Go to quiz!</Link>
    </div>
  );
}

export default withAuthRedirect(Results);
