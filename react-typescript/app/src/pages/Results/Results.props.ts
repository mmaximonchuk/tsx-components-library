import { DetailedHTMLProps, HTMLAttributes } from 'react';

export interface iResultProps extends DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement> {

}