import { useEffect, useState } from "react";
import { useParams, Link } from "react-router-dom";
import cn from "classnames";

import { useAppDispatch, useAppSelector } from "../../hooks/useStore";
import { setCurrentQuizId, setStep, setUserAnswer } from "../../store/quizSlice/quizReducer";

import { iQuizOfferedAnswer, iQuizData } from "../../utils/mockQuizData";
import { Routes } from "../../utils/routes";
import { iDirectionLink, StepDirections } from "./QuizStep.props";

import withQuizPageRedirect from "../../hoc/withQuizPageRedirect";
import withAuthRedirect from "../../hoc/withAuthRedirect";
import { Answer, Loader } from "../../components";

import s from "./QuizStep.module.scss";

function QuizStep(): JSX.Element {
  const dispatch = useAppDispatch();
  const { quizId, stepId } = useParams();
  // const { user } = useAuthContext();
  const { currentStep, currentQuizId, isFetching, currentUserAnswers, currentQuizData } = useAppSelector(
    (state) => state.quiz
  );
  const [presentQuiz, setPresentQuiz] = useState<iQuizData | null>(null);

  const onSelectAnswer = ({ answerId }) => {
    dispatch(setUserAnswer({ currentStep, answerId }));
  };

  useEffect(() => {
    const handleStageChange = (stepId: string, quizId: string) => {
      dispatch(setStep({ stepId: Number.parseInt(stepId) }));
      dispatch(setCurrentQuizId({ quizId: Number.parseInt(quizId) }));
    };

    handleStageChange(stepId, quizId);
  }, [dispatch, quizId, stepId, isFetching]);

  useEffect(() => {
    if (currentQuizData === null) return;

    const presentQuiz = currentQuizData?.find((quiz) => quiz._id === currentQuizId) ?? null;
    setPresentQuiz(presentQuiz);
  }, [currentQuizData, currentQuizId]);

  const defineStageLink = (direction: StepDirections): string => {
    if (!currentStep) return "";

    const isLastQuestion = currentStep === presentQuiz?.questions.length;
    const isFirstQuestion = currentStep === 1;

    const nextPageRoute = isLastQuestion ? Routes.ResultsPage : `/${quizId}/step/${currentStep + 1}`;
    const previousPageRoute = isFirstQuestion ? Routes.HomePage : `/${quizId}/step/${currentStep - 1}`;

    if (direction === StepDirections.Forward) {
      return nextPageRoute;
    }

    return previousPageRoute;
  };

  const activeAnswer = currentUserAnswers
    .find((answer) => answer.quizId === currentQuizId)
    ?.answers.map((answer) => ({ stepId: answer.stepId, answersId: [...answer.answersId] }));

  const accessibleNextStepLink = activeAnswer?.some((step) => step?.stepId === currentStep);

  const showNextStageLink = presentQuiz && Boolean(currentStep) && accessibleNextStepLink;
  const showPreviousStageLink = presentQuiz && Boolean(currentStep);
  // TODO: SIGN IN
  // const showQuestions = isFetching === false && user !== null;
  const showQuestions = isFetching === false && true;
  const allQuestionsCount = presentQuiz?.questions.length;
  return (
    <div className={s.wrapper}>
      <div className={s.stepMeta}>
        <h2 className={s.stepQuizTitle}>Quiz {quizId} </h2>
        <h4 className={s.stepQuizDescription}>
          Step {stepId} / {allQuestionsCount}
        </h4>
      </div>

      <div className={s.directionLinkWrapper}>
        {showPreviousStageLink && (
          <DirectionLink
            direction={StepDirections.Backward}
            presentQuiz={presentQuiz}
            stageLink={() => defineStageLink(StepDirections.Backward)}
          />
        )}
      </div>
      <div className={s.currentQuestionWrapper}>
        {showQuestions ? (
          presentQuiz?.questions[stepId - 1].answers.map((question: iQuizOfferedAnswer) => {
            const isActive =
              activeAnswer?.filter((step) => step.stepId === currentStep)[0]?.answersId.includes(question._id) ?? false;

            return (
              <Answer
                key={question._id}
                presentQuiz={presentQuiz}
                className={cn(s.questionWrapper, {
                  [s.active]: isActive,
                })}
                isActive={isActive}
                onSelectAnswer={onSelectAnswer}
                {...question}
              >
                {question.text}
              </Answer>
            );
          })
        ) : (
          <div className={s.loader}>
            <Loader type="fast-spinner" />
          </div>
        )}
      </div>
      <div className={cn(s.directionLinkWrapper, s.linkFurther)}>
        {showNextStageLink && (
          <DirectionLink
            direction={StepDirections.Forward}
            presentQuiz={presentQuiz}
            stageLink={() => defineStageLink(StepDirections.Forward)}
          />
        )}
      </div>
    </div>
  );
}

export default withAuthRedirect(withQuizPageRedirect(QuizStep));

const DirectionLink = ({ stageLink, direction }: iDirectionLink) => {
  return (
    <Link className={s.directionLink} to={stageLink}>
      {direction === StepDirections.Forward ? "Go further >>" : "<< Previous question"}
    </Link>
  );
};
