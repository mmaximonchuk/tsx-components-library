import { HTMLAttributes, DetailedHTMLProps } from "react";
// import { iQuizContext } from "../store/quizContext";
import { iQuizData } from "../../utils/mockQuizData";

export interface IQuizStep extends DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement> {}

export interface iDirectionLink
  extends DetailedHTMLProps<HTMLAttributes<HTMLAnchorElement>, HTMLAnchorElement> {
  stageLink: () => string;
  direction: StepDirections;
  presentQuiz: iQuizData | null;
}

export enum StepDirections {
  Forward,
  Backward,
}
