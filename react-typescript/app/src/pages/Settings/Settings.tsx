import { useEffect, useState } from "react";

import { setBgColor, setFontColor, setFontSize } from "../../store/userSlice/userReducer";

import withAuthRedirect from "../../hoc/withAuthRedirect";
import { useAppDispatch, useAppSelector } from "../../hooks/useStore";

import s from "./Settings.module.css";

function Settings(): JSX.Element {
  const user = useAppSelector((state) => state.user.user);
  // const fontSize = useAppSelector((state) => state.user.fontSize);
  // const bgColor = useAppSelector((state) => state.user.bgColor);
  // const fontColor = useAppSelector((state) => state.user.fontColor);

  // const [localFontSize, setLocalFontSize] = useState<number>(fontSize);
  // const [localFontColor, setLocalFontColor] = useState<string>(fontColor);
  // const [localBgColor, setLocalBgColor] = useState<string>(bgColor);
  // const dispatch = useAppDispatch();

  // useEffect(() => {
  //   dispatch(setFontSize({ fontSize: localFontSize }));
  // }, [dispatch, localFontSize]);

  // useEffect(() => {
  //   dispatch(setBgColor({ bgColor: localBgColor }));
  // }, [dispatch, localBgColor]);

  // useEffect(() => {
  //   dispatch(setFontColor({ fontColor: localFontColor }));
  // }, [dispatch, localFontColor]);

  const showUserDetails = user !== null;
  const photoURL = user?.photoURL;
  const displayName = user?.displayName;
  const email = user?.email;
  return (
    <div className={s.wrapper}>
      {showUserDetails && (
        <div className={s.userData}>
          <img className={s.userAvatar} src={photoURL ?? ""} alt={displayName ?? "user-avatar"} />
          <div className={s.userDetails}>
            <p>
              <span>Email:</span> {email}
            </p>
            <p>
              <span>Display Name:</span> {displayName}
            </p>
          </div>
        </div>
      )}
      {/* <div className={s.settings}>
        <label className={s.inputGroup}>
          <span style={{ fontSize: `${fontSize}px` }} className={s.inputTip}>
            Set font size: {fontSize}
          </span>
          <input
            onChange={(e) => setLocalFontSize(parseInt(e.target.value))}
            value={fontSize}
            min={16}
            max={24}
            type="range"
            name="font-size"
            id="font-size"
          />
        </label>
        <label className={s.inputGroup}>
          <span className={s.inputTip}>Background color:</span>
          <input
            onChange={(e) => setLocalBgColor(e.target.value)}
            value={bgColor}
            type="color"
            name="bg-color"
            id="bg-color"
          />
        </label>
        <label className={s.inputGroup}>
          <span className={s.inputTip}>Font color:</span>
          <input
            onChange={(e) => setLocalFontColor(e.target.value)}
            value={fontColor}
            type="color"
            name="font-color"
            id="font-color"
          />
        </label>
      </div> */}
    </div>
  );
}

export default withAuthRedirect(Settings);
