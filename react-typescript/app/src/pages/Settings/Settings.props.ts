import { DetailedHTMLProps, HTMLAttributes } from 'react';

export interface iSettingsProps extends DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement> {

}