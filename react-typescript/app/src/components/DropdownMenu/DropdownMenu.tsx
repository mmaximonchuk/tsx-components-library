import { Link } from 'react-router-dom';
import cn from 'classnames';
import { FiSettings, FiLogOut } from "react-icons/fi";
import { MdOutlineQuiz } from "react-icons/md";
import { ImPencil2 } from "react-icons/im";

import { DropdownMenuProps } from './DropdownMenu.props';

import { Routes } from '../../utils/routes';

import s from './DropdownMenu.module.scss';

export default function DropdownMenu({ isVisibleMenu, onLogout }: DropdownMenuProps) {
    return (
        <div
            className={cn(s.dropdown, {
                [s.visible]: isVisibleMenu,
            })}
        >
            <ul className={s.dropdownList}>
                <li className={s.dropdownListItem}>
                    <Link to={Routes.BlogPage}>
                        <ImPencil2 size="2.5rem" />
                        Blog
                    </Link>
                </li>
                <li className={s.dropdownListItem}>
                    <Link to={Routes.HomePage}>
                        <MdOutlineQuiz size="2.5rem" />
                        {" "}
                        Quizes
                    </Link>
                </li>
                <li className={s.dropdownListItem}>
                    <Link to={Routes.SettingsPage}>
                        <FiSettings size="2.5rem" />
                        {" "}
                        Settings
                    </Link>
                </li>
                <li className={s.dropdownListItem}>
                    <button onClick={onLogout}>
                        <FiLogOut size="2.5rem" />
                        Log Out
                    </button>
                </li>
            </ul>
        </div>
    )
}
