import { DetailedHTMLProps, HTMLAttributes } from "react";

export interface DropdownMenuProps extends DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement> {
  isVisibleMenu: boolean;
  onLogout: () => void;
}
