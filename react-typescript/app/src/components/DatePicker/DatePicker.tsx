import { useMemo, useState } from "react";

import s from "./DatePicker.module.scss";

interface DatePickerProps {
  value: Date;
  onChange: (value: Date) => void;
  min?: Date;
  max?: Date;
}

interface DateCellItem {
  date: number;
  month: number;
  year: number;

  // ????
  isToday?: boolean;
  isSelected?: boolean;
}

const months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
const daysOfTheWeek = ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"];

const getDaysAmmountInAMonth = (year: number, month: number) => {
  const nextMonthDate = new Date(year, month + 1, 1);
  // mutates the date object
  nextMonthDate.setMinutes(-1);
  return nextMonthDate.getDate();
};

const getPreviousMonthDays = (year: number, month: number) => {
  const currentMonthFirstDay = new Date(year, month, 1);
  const dayOfTheWeek = currentMonthFirstDay.getDay();
  //TODO: here comes BUG
  const prevMonthCellsAmmount = dayOfTheWeek - 1;

  const daysAmmountInPrevMonth = getDaysAmmountInAMonth(year, month - 1);

  const dateCells: DateCellItem[] = [];

  const [cellYear, cellMonth] = month === 0 ? [year - 1, 11] : [year, month];

  for (let i = prevMonthCellsAmmount - 1; i >= 0; i--) {
    dateCells.push({
      year: cellYear,
      month: cellMonth,
      date: daysAmmountInPrevMonth - i,
    });
  }
  return dateCells;
};

const VISIBLE_CELLS_AMMOUNT = 7 * 6; // 42

const getNextMonthDays = (year: number, month: number) => {
  // TODO: copy paste
  const currentMonthFirstDay = new Date(year, month, 1);
  const dayOfTheWeek = currentMonthFirstDay.getDay();
  const prevMonthCellsAmmount = 7 - dayOfTheWeek;
  // TODO: end copy paste

  const daysAmmount = getDaysAmmountInAMonth(year, month);

  const nextMonthdays = VISIBLE_CELLS_AMMOUNT - daysAmmount - prevMonthCellsAmmount;

  const [cellYear, cellMonth] = month === 11 ? [year + 1, 0] : [year, month + 1];

  const dateCells: DateCellItem[] = [];

  for (let i = 1; i <= nextMonthdays; i++) {
    dateCells.push({
      year: cellYear,
      month: cellMonth,
      date: i,
    });
  }
  return dateCells;
};

const getCurrentMonthDays = (year: number, month: number, numberOfDays: number) => {
  const dateCells: DateCellItem[] = [];

  for (let i = 1; i <= numberOfDays; i++) {
    dateCells.push({
      year,
      month,
      date: i,
    });
  }

  return dateCells;
};

function DatePicker({ value, onChange }: DatePickerProps) {
  const [panelYear, setPanelYear] = useState(() => value.getFullYear());
  const [panelMonth, setPanelMonth] = useState(() => value.getMonth());

  const [year, month, day] = useMemo(() => {
    const currentYear = value.getFullYear();
    const currentDay = value.getDate();
    const currentMonth = value.getMonth();

    return [currentYear, currentMonth, currentDay];
  }, [value]);

  const dateCells = useMemo(() => {
    // const items: DateCellItem[] = [];

    // const panelDate = new Date(panelYear, panelMonth, );
    const daysInAMonth = getDaysAmmountInAMonth(panelYear, panelMonth);

    const previousMonthCells = getPreviousMonthDays(panelYear, panelMonth);
    const currentMonthCells = getCurrentMonthDays(panelYear, panelMonth, daysInAMonth);
    const nextMonthCells = getNextMonthDays(panelYear, panelMonth);

    return [...previousMonthCells, ...currentMonthCells, ...nextMonthCells];
  }, [panelYear, panelMonth]);

  const onDateSelect = (item: DateCellItem) => {
    onChange(new Date(item.year, item.month, item.date));
  };

  const nextYear = () => {
    setPanelYear((currentYear) => currentYear + 1);
  };
  const prevYear = () => {
    setPanelYear((currentYear) => currentYear - 1);
  };

  const nextMonth = () => {
    if (panelMonth !== 11) {
      setPanelMonth((currentMonth) => currentMonth + 1);
      return;
    }

    setPanelMonth(0);
    setPanelYear((currentYear) => currentYear + 1);
  };
  const prevMonth = () => {
    if (panelMonth !== 0) {
      setPanelMonth((currentMonth) => currentMonth - 1);
      return;
    }

    setPanelMonth(11);
    setPanelYear((currentYear) => currentYear - 1);
  };

  return (
    <div className={s.Calendar}>
      {/* Date
      <div>
        {day} {month} {year}
      </div> */}
      <div className={s.CalendarControls}>
        <button onClick={prevYear}>Prev Year</button>
        <button onClick={prevMonth}>Prev Month</button>
        ||
        <button onClick={nextMonth}>Next Month</button>
        <button onClick={nextYear}>Next Year</button>
      </div>
      <div className={s.CalendarPanel}>
        {daysOfTheWeek.map((weekDay) => (
          <div key={weekDay} className={s.CalendarPanelItem}>
            {weekDay}
          </div>
        ))}
        {dateCells.map((cell, index) => {
          const isCurrentDate = cell.year === year && cell.month === month && cell.date === day;
          return (
            <div key={`${index}-${cell.date}-${cell.month}-${cell.year}`} onClick={() => onDateSelect(cell)} className={[s.CalendarPanelItem, isCurrentDate ? s.CalendarCurrentDate : ""].join(" ")}>
              {cell.date}
            </div>
          );
        })}
      </div>
    </div>
  );
}

export default DatePicker;
