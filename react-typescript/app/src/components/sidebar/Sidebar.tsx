import cn from 'classnames';
import { Card } from '..';
import { iUser } from '../../domain/iUser.interface'

interface iSidebarProps {
	user: iUser | null;
	className: string;
	logOut: () => void;
}

export const Sidebar = ({ user, className, logOut }: iSidebarProps): JSX.Element => {
	const displayName = user?.displayName;
	const photoURL = user?.photoURL;
	const email = user?.email;

	return (
		<aside className={cn(className)}>
			{user !== undefined && <Card logOut={logOut} displayName={displayName} photoURL={photoURL} email={email} />}
		</aside>
	)
}
