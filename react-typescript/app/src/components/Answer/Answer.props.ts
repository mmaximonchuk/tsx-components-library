import { DetailedHTMLProps, HTMLAttributes } from "react";
import { iQuizOfferedAnswer } from "../../utils/mockQuizData";

export interface iAnswerProps
  extends iQuizOfferedAnswer,
    DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement> {
  isActive: boolean;
  onSelectAnswer: ({ answerId }: { answerId: number }) => void;
}
