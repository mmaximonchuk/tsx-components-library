
import cn from "classnames";

import { iQuizData } from "../../utils/mockQuizData";

import { iAnswerProps } from "./Answer.props";

import s from "./Answer.module.scss";

export default function Answer({
  className,
  text,
  _id,
  presentQuiz,
  isActive = false,
  onSelectAnswer,
  ...restProps
}: iAnswerProps & { presentQuiz: iQuizData }) {
  
  const handleSelectAnswer = () => {
    onSelectAnswer({ answerId: _id })
  }

  return (
    <div className={className} onClick={handleSelectAnswer}>
      <div
        className={cn(s.selectedCircle, {
          [s.visible]: isActive,
        })}
      ></div>
      <span>{text}</span>
    </div>
  );
}
