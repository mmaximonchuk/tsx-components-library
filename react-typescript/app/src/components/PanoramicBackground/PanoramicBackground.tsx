import { Pannellum } from "pannellum-react";

import imgBackgroung from "../../assets/images/scenic_forest_mountains_sunrise_flying_birds.jpg";

function PanoramicBackground({ className }) {
  return (
    <Pannellum
      id={className}
      author="Maxi"
      title=""
      orientationOnByDefault={false}
      keyboardZoom
      preview=""
      previewAuthor="Maxi tg@never_be_typical"
      previewTitle=""
      hotspotDebug={false}
 
      width="100%"
      height="100%"
      image={imgBackgroung}
      yaw={300}
      hfov={110}
      autoLoad
      autoRotate={-5}
      showZoomCtrl={false}
      mouseZoom={false}
      onLoad={() => {
        console.log("panorama loaded");
      }}
    ></Pannellum>
  );
}

export default PanoramicBackground;
