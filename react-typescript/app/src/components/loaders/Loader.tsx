import { LoaderProps } from './Loader.props';
import cn from 'classnames';


import s from "./Loader.module.css";

export const Loader = ({ type, className }: LoaderProps) => {

  switch (type) {
    case 'red':
      return (
        <div className={cn(s.loader, className, {
          [s.loaderRed]: type === 'red'
        })}>
          <div className={s.line}>
            <div className={s.line}>
              <div className={s.line}>
                <div className={s.line}>
                  <div className={s.line}>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      )
    case 'green':
      return (
        <div className={cn(s.loader, className, {
          [s.loaderGreen]: type === 'green'
        })}>
                   <div className={s.line}>
            <div className={s.line}>
              <div className={s.line}>
                <div className={s.line}>
                  <div className={s.line}>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      )
    case 'blue':
      return (
        <div className={cn(s.loader, className, {
          [s.loaderBlue]: type === 'blue'
        })}>
                   <div className={s.line}>
            <div className={s.line}>
              <div className={s.line}>
                <div className={s.line}>
                  <div className={s.line}>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      )
    case 'fast-spinner': 
      return <div className={cn(s.hourglass, className)}></div>
    default:
      return (
        <></>
      )
  }
};
