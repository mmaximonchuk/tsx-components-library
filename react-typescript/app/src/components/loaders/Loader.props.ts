import { DetailedHTMLProps, HTMLAttributes } from 'react';

export interface LoaderProps extends DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement> {
	type: "green" | "red" | "blue" | 'fast-spinner',
}
