import { lazy } from "react";

export { default as Answer } from "./Answer/Answer";
export { Card } from "./card/Card";
export { Loader } from "./loaders/Loader";
export { Sidebar } from "./sidebar/Sidebar";
export const PanoramicBG = lazy(() => import("./PanoramicBackground/PanoramicBackground"));

export { default as DropdownMenu } from "./DropdownMenu/DropdownMenu";
export { default as DatePicker } from "./DatePicker/DatePicker";
