import { DetailedHTMLProps, HTMLAttributes } from 'react';

export interface CardProps extends DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement> {
	displayName: string | null | undefined;
	photoURL: string | null | undefined;
	email: string | null | undefined;
	logOut: (() => void) | undefined;
}