import { useState } from "react";
import cn from 'classnames';
import { CardProps } from './Card.props';
import { AiOutlineUser, AiOutlineDown } from 'react-icons/ai';
import { IoChatboxOutline } from 'react-icons/io5';
import { FiSettings, FiLogOut } from 'react-icons/fi';
import { BiHelpCircle } from 'react-icons/bi';

import s from "./Card.module.css";

export const Card = ({ photoURL, displayName, email, logOut }: CardProps): JSX.Element => {
  const [isCardOpen, setIsCardOper] = useState<boolean>(false);

  const shouldRoundDisplayName = displayName !== null && displayName !== undefined && displayName?.length > 13;
  const roundDisplayName = shouldRoundDisplayName ? displayName?.slice(0, 16) + '...' : displayName;

  return (
    <div className={cn(s.card, {
      [s.active]: isCardOpen
    })}>
      <div className={s.content}>
        <div className={s.imgBx}>
          <img
            src={photoURL === null ? '' : photoURL}
            // src="https://images.pexels.com/photos/3464632/pexels-photo-3464632.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500"
            alt={displayName === null ? '' : displayName}
          />
        </div>
        <h2 className={s.title}>
          {roundDisplayName} <br /> <span>{email}</span>{" "}
        </h2>
      </div>
      <ul className={s.navigation}>
        <li>
          <a href="google.com">
            <AiOutlineUser size="2.5rem" />
            Edit profile
          </a>
        </li>
        <li>
          <a href="google.com">
            <IoChatboxOutline size="2.5rem" />
            Inbox
          </a>
        </li>
        <li>
          <a href="google.com">
            <FiSettings size="2.5rem" />
            Settings
          </a>
        </li>
        <li>
          <a href="google.com">
            <BiHelpCircle size="2.5rem" />
            Support
          </a>
        </li>
        <li>
          <button onClick={logOut}>
            <FiLogOut size="2.5rem" />
            Logout
          </button>
        </li>
      </ul>
      <div onClick={() => setIsCardOper(!isCardOpen)} className={s.toggle}>
        <AiOutlineDown size="2.5rem" />
      </div>
    </div >
  );
};
