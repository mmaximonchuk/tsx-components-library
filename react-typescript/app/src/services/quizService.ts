import axios from "axios";
import { iQuizData } from "../utils/mockQuizData";

const instance = axios.create({
  baseURL: "http://localhost:5000/",
  headers: { "Content-Type": "applicateion/json" },
});

export const quizServiceAPI = {
  async getQuizes(): Promise<iQuizData[]> {
    const { data } = await instance.get<iQuizData[]>("quizes");

    return data;
  },
};
