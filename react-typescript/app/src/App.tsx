import { useEffect } from "react";
import { Route, useLocation } from "react-router-dom";
// import { doc, DocumentData, getDoc } from "firebase/firestore";
import { onAuthStateChanged } from "firebase/auth";
import { message } from "antd";

import { HomePage, Login, QuizStep, Results, Settings } from "./pages";
import { Layout } from "./Layout";
import { Loader } from "./components";

import { iUser } from "./domain/iUser.interface";

import { auth } from "./auth/base";

import {
  setCurrentQuizData,
  setCurrentQuizId,
  setIsFetching,
  setStep,
  setUserProgress,
} from "./store/quizSlice/quizReducer";
import { setAuthStatus, setUser } from "./store/userSlice/userReducer";

import { useAppDispatch, useAppSelector } from "./hooks/useStore";

import { quizServiceAPI } from "./services/quizService";

import { Routes } from "./utils/routes";
import { SUCCESSFULLY_LOGGED_IN_MESSAGE } from "./utils/constants";
import { getErrorMessage } from "./utils/helpers";


function App(): JSX.Element {
  // const { fontSize, bgColor, fontColor, user, status } = useAppSelector((state) => state.user);
  // const authUser = useAppSelector((state) => state.user.user);
  const authenticated = useAppSelector((state) => state.user.authenticated);
  const status = useAppSelector((state) => state.user.status);
  // const error = useAppSelector((state) => state.user.error);
  const currentStep = useAppSelector((state) => state.quiz.currentStep);
  const dispatch = useAppDispatch();
  const location = useLocation();
  const nullishQuizData = !location.pathname.includes("step");

  useEffect(() => {
    const listener = onAuthStateChanged(auth, (user) => {
      if (user === null) return;

      const serializableUserData: iUser = {
        uid: user.uid,
        email: user.email,
        displayName: user.displayName,
        photoURL: user.photoURL,
      };

      dispatch(setUser({ user: serializableUserData }));
      dispatch(setAuthStatus({ status: "resolved" }));
      message.success(SUCCESSFULLY_LOGGED_IN_MESSAGE);
    });

    return listener;
  }, [dispatch]);

  useEffect(() => {
    if(!authenticated) return;
    const fetchQuizes = async () => {
      dispatch(setIsFetching({ isFetching: true }));
      try {
        const quizData = await quizServiceAPI.getQuizes();
        dispatch(setCurrentQuizData({ quizData }));
      } catch (e) {
        const errMessage = getErrorMessage(e);

        message.error({ type: "error", content: errMessage });
      } finally {
        dispatch(setIsFetching({ isFetching: false }));
      }
    };

    fetchQuizes();
  }, [dispatch, authenticated]);

  useEffect(() => {
    if (currentStep === null) return;

    dispatch(setUserProgress());
  }, [dispatch, currentStep]);

  useEffect(() => {
    if (!nullishQuizData) return;

    dispatch(setStep({ stepId: null }));
    dispatch(setCurrentQuizId({ quizId: null }));
    dispatch(setUserProgress());
  }, [dispatch, nullishQuizData]);

  // useEffect(() => {
  // const html = document.querySelector("html") as HTMLElement;
  // html.style.fontSize = fontSize + "px";
  // }, [fontSize]);

  // useEffect(() => {
  //   document.documentElement.style.setProperty("--fontPrimaryColor", fontColor);
  //   document.documentElement.style.setProperty("--fontPrimaryTitleColor", `${fontColor}B3`);
  // }, [fontColor]);

  // const array = [
  //   { label: 'label1', id: 1, des: 'des label1' },
  //   { label: 'label2', id: 1, des: 'des label2' },
  //   { label: 'label3', id: 1, des: 'des label3' },
  //   { label: 'label4', id: 1, des: 'des label4' },
  //   { label: 'label5', id: 1, des: 'des label5' },
  // ]

  // function generateNewArrayByKey<T>(arr: T[], key: keyof T) {
  //   return arr.map(item => ({ [key]: item[key] }))
  // }

  // console.log(generateNewArrayByKey(array, "id"));

  if (status === "loading") {
    return (
      <div>
        <Loader type="fast-spinner" />
      </div>
    );
  }

  return (
    <Layout
    // bgColor={bgColor}
    >
      <Route path={Routes.Login} exact>
        <Login />
      </Route>
      <Route path={Routes.HomePage} exact>
        <HomePage />
      </Route>
      <Route path={Routes.QuizStep} exact>
        <QuizStep />
      </Route>
      <Route path={Routes.ResultsPage} exact>
        <Results />
      </Route>
      <Route path={Routes.SettingsPage} exact>
        <Settings />
      </Route>
    </Layout>
  );
}
export default App;
