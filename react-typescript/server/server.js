const http = require("http");

const PORT = 3003;

const server = http.createServer((req, res) => {
  switch (req.url) {
    case "/":
    case "/quizes": {
      res.write("Quizes UwU <3");
      break;
    }
    case "/profile": {
      res.write("Profile UwU <3");
      break;
    }
    default: {
      res.write("404 not found");
      break;
    }
  }
  
  res.end();
});

server.listen(PORT || 3003);
